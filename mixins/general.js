import Vue from 'vue'
import { STATUS, JURUSAN, TYPE_CATEGORY, STATUS_POST } from '@/utils/constants'
import moment from 'moment'

Vue.mixin({
  methods: {
    getType(data) {
      var condition = ''

      for (const item of TYPE_CATEGORY) {
        if (item.id === data) condition = item.name
      }
      return condition
    },
    getStatus(data) {
      var condition = ''

      for (const item of STATUS) {
        if (item.id === data) condition = item
      }
      return condition
    },
    getStatusPost(data) {
      var condition = ''

      for (const item of STATUS_POST) {
        if (item.id === data) condition = item
      }
      return condition
    },
    getJurusan(data) {
      var condition = {}

      for (const item of JURUSAN) {
        if (item.id === data) condition = item
      }
      return condition
    },
    getProvince(regencies, kota_id) {
      var id = 0
      var name = ''

      for (var item in regencies) {
        if (regencies[item].id === kota_id) {
          id = regencies[item].province.id
          name = regencies[item].province.name
        }
      }

      return {
        id,
        name,
      }
    },
    dateFull(data, format = null) {
      var defaultFormat = 'yyyy-MM-DD hh:mm:ss'

      if (format) {
        var defaultFormat = format
      }

      return moment.utc(String(data)).format(defaultFormat)
    },
    strHumanize(str) {
      var i,
        frags = str.split('_')
      for (i = 0; i < frags.length; i++) {
        frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1)
      }
      return frags.join(' ')
    },
    setErrorRequiredListHtml(data) {
      let title = '<div class="title">Failed</div>'

      let html = ''

      html += '<ul>'
      for (let item in data) {
        html +=
          '<li>The&nbsp;' +
          this.strHumanize(data[item]) +
          '&nbsp;field is required</li>'
      }
      html += '</ul>'
      return html
    },
  },
})

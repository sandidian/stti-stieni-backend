import axios from 'axios'

export const state = () => ({
  user: null,
  createUser: null,
  userDetail: null,
  updateUser: null,
  deleteUser: null,
  userProfile: null,
  editProfile: null,
})

export const mutations = {
  SET_USER(state, user) {
    state.user = user
  },
  SET_USER_DETAIL(state, userDetail) {
    state.userDetail = userDetail
  },
  CREATE_USER(state, createUser) {
    state.createUser = createUser
  },
  UPDATE_USER(state, updateUser) {
    state.updateUser = updateUser
  },
  DELETE_USER(state, deleteUser) {
    state.deleteUser = deleteUser
  },
  SET_USER_PROFILE(state, userProfile) {
    state.userProfile = userProfile
  },
  EDIT_PROFILE(state, editProfile) {
    state.editProfile = editProfile
  },
}

export const actions = {
  async list({ commit, dispatch }) {

    await this.$axios.get(`/api/v1/admin/users`).then(function (response) {

      if (response.status === 200 && response.data.success === true)
        commit('SET_USER', response.data.data)
      else
        throw new Error(response.data.message)

    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })

  },
  async createUser({ commit, dispatch }, { data }) {
    await this.$axios({
      method: 'post',
      url: '/api/v1/admin/users/create',
      data: data,
    }).then(function (response) {

      if (response.status === 200 && response.data.success === true)
        commit('CREATE_USER', response.data)
      else
        throw new Error(response.data.message)
    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })
  },
  async updateUser({ commit, dispatch }, { id, data }) {

    await this.$axios({
      method: 'post',
      url: `/api/v1/admin/users/update/${id}`,
      data: data,
    }).then(function (response) {
      console.log(response)
      if (response.status === 200 && response.data.success === true)
        commit('UPDATE_USER', response.data)
      else
        throw new Error(response.data.message)
    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })
  },
  async deleteUser({ commit, dispatch }, { id }) {

    await this.$axios({
      method: 'post',
      url: `/api/v1/admin/users/delete/${id}`,
    }).then(function (response) {
      console.log(response)
      if (response.status === 200 && response.data.success === true)
        commit('DELETE_USER', response.data)
      else
        throw new Error(response.data.message)
    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })
  },
  async getUserDetail({ commit, dispatch }, { id }) {
    await this.$axios({
      method: 'get',
      url: `/api/v1/admin/users/edit/${id}`,
      params: { id: id },
    }).then(function (response) {
      if (response.status === 200 && response.data.success === true)
        commit('SET_USER_DETAIL', response.data.data)
      else
        throw new Error(response.data.message)
    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })
  },
  async userProfile({ commit, dispatch }) {
    const app = this
    await axios({
      method: 'get',
      url: '/api/user/profile',
    }).then(function (response) {
      if (response.status === 200 && response.data.success === true)
        commit('SET_USER_PROFILE', response.data)

      else
        throw new Error(response.data.message)
    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })
  },
  async editProfile({ commit, dispatch }, { data }) {
    const app = this
    await axios({
      method: 'put',
      url: `/api/user/edit-profile`,
      data: data,
    }).then(function (response) {
      if (response.status === 200 && response.data.success === true)
        commit('EDIT_PROFILE', response.data)

      else
        throw new Error(response.data.message)
    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })
  },
}

export const getters = {
  getUser: (state) => {
    return state.user
  },
  getCreateUser: (state) => {
    return state.createUser
  },
  getUserDetail: (state) => {
    return state.userDetail
  },
  getUpdateUser: (state) => {
    return state.updateUser
  },
  getDeleteUser: (state) => {
    return state.deleteUser
  },
  getUserProfile: (state) => {
    return state.userProfile
  },
  getEditProfile: (state) => {
    return state.editProfile
  },
}

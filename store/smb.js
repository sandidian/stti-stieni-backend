export const state = () => ({
  mahasiswa: null,
  create: null,
  detail: null,
  update: null,
  delete: null,
})

export const mutations = {
  SET_MAHASISWA(state, mahasiswa) {
    state.mahasiswa = mahasiswa
  },
  SET_DETAIL(state, detail) {
    state.detail = detail
  },
  SET_CREATE(state, create) {
    state.create = create
  },
  SET_UPDATE(state, update) {
    state.update = update
  },
  SET_DELETE(state, remove) {
    state.delete = remove
  },

}

export const actions = {
  async list({ commit, dispatch }) {

    await this.$axios.get(`/api/v1/admin/mahasiswa-baru`).then(function (response) {

      if (response.status === 200 && response.data.success === true)
        commit('SET_MAHASISWA', response.data.data)
      else
        throw new Error(response.data.message)

    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })

  },
  async create({ commit, dispatch }, { data }) {
    await this.$axios({
      method: 'post',
      url: '/api/v1/admin/mahasiswa-baru/create',
      data: data,
    }).then(function (response) {
      if (response.status === 200 && response.data.success === true)
        commit('SET_CREATE', response.data)
      else
        throw new Error(response.data.message)
    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })
  },
  async update({ commit, dispatch }, { id, data }) {

    await this.$axios({
      method: 'post',
      url: `/api/v1/admin/mahasiswa-baru/update/${id}`,
      data: data,
    }).then(function (response) {
      console.log(response)
      if (response.status === 200 && response.data.success === true)
        commit('SET_UPDATE', response.data)
      else
        throw new Error(response.data.message)
    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })
  },
  async delete({ commit, dispatch }, { id }) {

    await this.$axios({
      method: 'post',
      url: `/api/v1/admin/mahasiswa-baru/delete/${id}`,
    }).then(function (response) {
      console.log(response)
      if (response.status === 200 && response.data.success === true)
        commit('SET_DELETE', response.data)
      else
        throw new Error(response.data.message)
    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })
  },
  async detail({ commit, dispatch }, { id }) {
    await this.$axios({
      method: 'get',
      url: `/api/v1/admin/mahasiswa-baru/edit/${id}`,
      params: { id: id },
    }).then(function (response) {
      if (response.status === 200 && response.data.success === true)
        commit('SET_DETAIL', response.data.data)
      else
        throw new Error(response.data.message)
    }).catch(function (error) {
      if (error.response === undefined)
        throw error
      else
        throw new Error('Network Communication Error')
    })

  },
}

export const getters = {
  getMahasiswa: (state) => {
    return state.mahasiswa
  },
  getCreate: (state) => {
    return state.create
  },
  getDetail: (state) => {
    return state.detail
  },
  getUpdate: (state) => {
    return state.update
  },
  getDelete: (state) => {
    return state.delete
  },
}

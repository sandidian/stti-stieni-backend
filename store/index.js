export const state = () => ({
  region: [],
  changePassword: null,
})

export const mutations = {
  SET_REGION(state, region) {
    state.region = region
  },
  CHANGE_PASSWORD(state, changePassword) {
    state.changePassword = changePassword
  },
}

export const getters = {
  getRegion: (state) => {
    return state.region
  },
  getChangePassword: (state) => {
    return state.changePassword
  },
}

export const actions = {
  async changePassword({ commit, dispatch }, { data }) {
    // console.log(data)
    await this.$axios({
      method: 'post',
      url: '/api/v1/admin/user/change-password',
      data: data,
    })
      .then(function (response) {
        // console.log(response.data)
        if (response.status === 200 && response.data.success === true)
          commit('CHANGE_PASSWORD', response.data)
        else throw new Error(response.data.message)
      })
      .catch(function (error) {
        if (error.response === undefined) throw error
        else throw new Error('Network Communication Error')
      })
  },
  async region({ commit, dispatch }) {
    await this.$axios
      .get(`/api/v1/regency`)
      .then(function (response) {
        if (response.status === 200 && response.data.success === true)
          commit('SET_REGION', response.data.data)
        else throw new Error(response.data.message)
      })
      .catch(function (error) {
        if (error.response === undefined) throw error
        else throw new Error('Network Communication Error')
      })
  },
}

export const STATUS = [
  {
    id: 1,
    text: 'Active',
    class: 'green',
  },
  {
    id: 2,
    text: 'Inactive',
    class: 'orange',
  },
  // {
  //   id: 3,
  //   text: 'Deleted',
  //   class: 'warning',
  // },
]

export const STATUS_POST = [
  {
    id: 1,
    name: 'Publish',
    class: 'green',
  },
  {
    id: 2,
    name: 'Draft',
    class: 'orange',
  },
  {
    id: 3,
    name: 'Archive',
    class: 'primary',
  },
]

export const JENIS_KELAMIN = [
  { id: 1, name: 'Laki-laki' },
  { id: 2, name: 'Perempuan' },
]

export const KAMPUS_STUDI = [
  { id: 1, name: 'Sekolah Tinggi Teknologi Indonesia(STTI)' },
  {
    id: 2,
    name: 'Sekolah Tinggi Ilmu Ekonomi Nasional Indonesia(STIENI)',
  },
]

export const JURUSAN = [
  { id: 1, kode: 201, name: 'Komputer Akuntansi', studi: 2 },
  { id: 2, kode: 202, name: 'Teknik Informatika', studi: 2 },
  { id: 3, kode: 203, name: 'Managemen Bisnis', studi: 2 },
  { id: 4, kode: 204, name: 'Managemen Perkantoran', studi: 2 },
  { id: 5, kode: 205, name: 'Design Grafis', studi: 2 },
  { id: 6, kode: 206, name: 'Sekretaris', studi: 2 },
  { id: 7, kode: 207, name: 'Pulic Relation', studi: 2 },
  { id: 8, kode: 208, name: 'Bahasa Inggris', studi: 2 },
]

export const ASAL_LULUS = [
  { id: 1, name: 'D3/sejenis' },
  { id: 2, name: 'SMA/SMK/sejenis' },
  { id: 3, name: 'S1/sejenis' },
  { id: 4, name: 'D1/D2/Lainnya' },
]

export const PENANGGUNG_BIAYA = [
  { id: 1, name: 'Orang Tua/Wali ' },
  { id: 2, name: 'Beasiswa/Perusahaan' },
  { id: 3, name: 'Biaya Sendiri' },
  { id: 4, name: 'Kombinasi' },
]

export const SUMBER_INFO = [
  { id: 1, name: 'Agency' },
  { id: 2, name: 'Brosur' },
  { id: 3, name: 'Facebook' },
  { id: 4, name: 'Instagram' },
  { id: 5, name: 'Katalog' },
  { id: 6, name: 'Baligho' },
  { id: 7, name: 'Presentasi Sekolah' },
  { id: 8, name: 'Email' },
  { id: 9, name: 'Mini Banner' },
  { id: 10, name: 'Pameran' },
  { id: 11, name: 'SMS' },
  { id: 12, name: 'Koran' },
  { id: 13, name: 'Google' },
  { id: 14, name: 'Spanduk' },
  { id: 15, name: 'Aplikasi Android Info PTS' },
  { id: 16, name: 'Rekomendasi (GSF)' },
]

export const SUMBER_INFO_PRIMAGAMAS = [
  { id: 1, name: 'Koran' },
  { id: 2, name: 'Majalah' },
  { id: 3, name: 'Spanduk' },
  { id: 4, name: 'Banner' },
  { id: 5, name: 'Radio' },
  { id: 6, name: 'TV' },
  { id: 7, name: 'Teman/sdr' },
  { id: 8, name: 'Pameran' },
  { id: 9, name: 'Peresentasi Sekolah' },
]

export const TENOR = [
  { id: 1, name: '4 kali' },
  { id: 2, name: '12 kali' },
  { id: 3, name: '18 kali' },
  { id: 4, name: '24 kali' },
  { id: 5, name: '30 kali' },
  { id: 6, name: '36 kali' },
  { id: 7, name: '46 kali' },
  { id: 8, name: '48 kali' },
]

export const TYPE_CATEGORY = [
  { id: 1, name: 'Artikel' },
  { id: 2, name: 'Acara' },
  { id: 3, name: 'Gallery' },
]

export const TYPE_TAG = [{ id: 1, name: 'Artikel' }]

export const STATUS_ACTIVE = 1
export const STATUS_INCATIVE = 2
export const STATUS_DELETED = 3

export const STATUS_INCATIVE_NAME = 'INACTIVE'
export const STATUS_ACTIVE_NAME = 'ACTIVE'
export const STATUS_DELETED_NAME = 'DELETED'

export const STATUS_POST_BY_LABEL = { Publish: 1, Draft: 2, Archive: 3 }

export const TYPE_CATEGORY_ARTICLE = 1
export const TYPE_CATEGORY_EVENT = 2
export const TYPE_CATEGORY_GALLERY = 3

export const UPLOAD_STUDY = '/uploads/study/'
export const UPLOAD_ARTICLE = '/uploads/article/'
export const UPLOAD_EVENT = '/uploads/event/'
export const UPLOAD_GALLERY = '/uploads/gallery/'
